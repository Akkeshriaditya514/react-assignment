import React, { Component } from "react";
import { Button } from "semantic-ui-react";
import { connect } from "react-redux";
import { selectCompany } from "../actions";

class CompanySelect extends Component {
  renderList() {
    return this.props.company.map((company) => {
      return (
        <div key={company.name}>
          <Button
            className="ui Button"
            style={{ marginRight: "20px" }}
            onClick={() => this.props.selectCompany(company)}
          >
            {company.name}
          </Button>
        </div>
      );
    });
  }
  render() {
    return (
      <div style={{ margin: "40px" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",

            justifyContent: "flex-start",
          }}
        >
          {this.renderList()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { company: state.companies };
};

export default connect(mapStateToProps, { selectCompany })(CompanySelect);
